import bz2
class Reader:
    """Reads Package infos from Bzip File"""
    def __init__(self, file):
        self.__data = bz2.BZ2File(file).read()
        self.__pos = 0
        
    def getPackages(self):
        while self.__pos != -1:
            yield self.__readPackage()
        
    def __readPackage(self):
        pos = self.__data.find("\n\n", self.__pos)

        if pos == -1:
            self.__pos = -1
            return False
        
        pkgStr = self.__data[self.__pos:pos]
        self.__pos = pos + 2
        return self.__processPackage(pkgStr)
        
    def __processPackage(self, pkgStr):
        lines = pkgStr.strip().split("\n")
        pkg = {}
        for line in lines:
            if line in [chr(9),chr(32)]:
                #TODO: Need to add this to previous info
                four = 2 + 2
            else:
                try:
                    key, val = line.split(":", 1)
                    key = key.replace('-', '_')
                    pkg[key] = val.strip()
                except:
                    #TODO: Figure your non colon lines
                    pass
        
        return pkg