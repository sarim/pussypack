from peewee import Model, CharField, IntegerField
from options import db

class Package(Model):
    Package = CharField(null=True)
    Priority = CharField(null=True)
    Section = CharField(null=True)
    Installed_Size = IntegerField()
    Architecture = CharField(null=True)
    Source = CharField(null=True)
    Version = CharField(null=True)
    Depends = CharField(null=True)
    Filename = CharField(null=True)
    Size = IntegerField(null=True)
    MD5sum = CharField(null=True)
    Description = CharField(null=True)
    Homepage = CharField(null=True)

    class Meta:
            database = db
    
    @classmethod        
    def resetTable(self):
        db.execute_sql("Drop table Package");
        self.create_table(True)

Package.create_table(True)