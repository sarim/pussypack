import urllib2
import os
class Downloader():
    """Downloads Package List from Repo Url"""
    def __init__(self, url, repos, arch, path):
        self.url = url
        self.repos = repos
        self.arch = arch
        self.path = path
        
    def download(self):
        paths = []
        for repo in self.repos:
            paths.append(self.__downloadRepo(repo))
        return paths
        
    def __downloadRepo(self, repo):
        pkgUrl = "%s/%s/binary-%s/Packages.bz2" % (self.url, repo, self.arch)
        print pkgUrl
        bzData = urllib2.urlopen(pkgUrl).read()
        pkgPath = "%s/binary-%s" % (repo, self.arch)
        self.__saveFile(pkgPath, bzData)
        return pkgPath
        
    def __saveFile(self, filepath, data):
        try:
            os.makedirs(self.path + '/' + filepath)
        except:
            pass
        fd = open(self.path + '/' + filepath + "/Packages.bz2", "w")
        fd.write(data)
        fd.close()